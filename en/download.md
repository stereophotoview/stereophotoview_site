---
layout: default
title: 'Download'
lang: en
ref: download
permalink: en/download.html
order: 2
---
{% capture download_header %}Download{% endcapture download_header %}
{% capture download %}Download{% endcapture %}
{% assign Installer='Installer' %}
{% assign Portable='Portable' %}
{% assign any_linux='Any Linux' %}
{% assign appimage_note='To run the application, simply download this file, make it executable, and run. \
To integrate into your system you can use [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher).' %}
{% assign additionalDownloads='Additional downloads' %}
{% assign consoleConverter='Console Converter' %}
{% assign bit32='32 bit' %}
{% assign bit64='64 bit' %}
{% assign otherVersions='Other version' %}
{% assign debian_based='Ubuntu' %}
{% assign stableVersionOn='The latest stable version is available on' %}
{% assign installCommands='To install, run the following commands:' %}
{% assign instructions='Instructions' %}
{% assign rosa_repo='Packages for [ROSA](https://www.rosalinux.ru/) are provided by [djam](https://abf.io/djam) in his [personal repository](https://abf.io/platforms/djam_personal), where there are builds for i586 and x86_64 for ROSA Fresh R8. ' %}
{% assign rosa_example='Example of installing a package in a 64-bit version of ROSA Fresh R8:' %}
{% assign rosa_link='See the [repository page](https://abf.io/platforms/djam_personal) for a url to add media to your version of the system.' %}
{% assign sources='Sources' %}
{% assign archives='Archives' %}
{% assign gitRepo='Git repository' %}
{% assign lastStableVersion='The latest stable version' %}
{% assign buildingManual='Building manual' %}
{% assign version='Version' %}
{% assign readme_file='README.md' %}

{% include download.md %}
