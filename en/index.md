---
layout: default
title: 'About'
lang: en
ref: index
order: 0
--- 

Stereo Photo Viewer
===================

StereoPhotoView is a viewer/editor for stereoscopic 3d photo and video.

![StereoPhotoView](images/screenshorts/stereophotoview.png){:width="500px"}

Supported File Types
--------------------

### JPEG, JPS

An ordinary JPEG file with a stereo pair.\\
Can contain a stereo descriptor. In this case, the stereo source format is determined automatically when the file is opened.

### MPO

MPO files contains two separate JPEG images for the left and right view.

### Video files

Supports all [formats](https://ffmpeg.org/general.html#File-Formats){:target="_blank"} and [codecs](https://ffmpeg.org/general.html#Video-Codecs){:target="_blank"} supported by [FFmpeg](https://ffmpeg.org/) library.

Supported stereo formats
------------------------

![Stereo formats](images/screenshorts/stereophotoview-formats.png){:width="500px"}

Reading, saving, and displaying:

* Row interlaced;
* Column interlaced;
* Anaglyph red/cyan;
* Anaglyph yellow/blue;
* Side by side;
* Anamorphic side by side;
* Over/under;
* Anamorphic over/under;
* Mono;

Read and write only:

* Separate frames (MPO, separate JPEG files, or video files with two streams).

Supported devices
-----------------

* Anaglyph glasses.
* Monitors and TVs using passive polarization technology, such as LG.\\
    Use "Row interlaced" mode with the native resolution.
* Other 3D displays.\\
    Many 3D displays recognize different stereo formats.

Editing 3D images
---------------------

* Vertical/horizontal alignment, and rotation;
* Automatic vertical/horizontal alignment, and rotation;
* Cropping;
* Stereo format conversion;
* Views editing in external image editors.

![Alignment](images/screenshorts/stereophotoview-align.png){:width="500px"}
![Crop](images/screenshorts/stereophotoview-crop.png){:width="500px"}

Editing 3D videos
--------------------------------

* Constant or variable in time vertical and horizontal alignment (scene depth).;
* Stereo format conversion;

The result can be saved in one of the following formats: mp4, mkv, avi, flv.

Metadata describing the stereo format is added to the mp4 and mkv files.

Batch processing of stereo photos
--------------------------------

Batch processing allows you to process many photos.

![Batch processing of stereo photos](images/screenshorts/batch-processing.png){:width="600px"}


Command line mode
--------------------------------

The application includes a command line utility stereo-conv, which allows you to convert a stereo format and edit stereoscopic photo and video files.
To display help on usage, type the command line: ```stereo-conv -h``` or ```stereo-conv```.

Stereo Photo View accepts parameters that specify the format of the input file. To display help, type ```stereophotoview -h```.


Supported Operating Systems
---------------------------

* Windows 7 / 8 / 10
* Linux
