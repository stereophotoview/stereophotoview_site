---
layout: help
title: Opening a Stereo Pair or Video File
lang: en
parent: help
ref: help/file-open
permalink: en/help/file-open.html
order: 1
subrefs: [["file-open-single", "Opening a Stereo Pair From a Single File"],["file-open-separate","Opening a Stereo Pair From a Separate File"],["source-format", "Format of the Source"]]
---

## Opening a Stereo Pair or Video File ##

<a name="file-open-single"></a>
## Opening a Stereo Pair From a Single File ##

You can open a stereo file (JPS, JPEG, MPO) or a video file in the following ways:

* Via menu "File" - > "Open" or The ![Open]({{ site.baseurl }}/images/document-open.svg){:width="16px" :height="16px"} button on the toolbar.
* Drag and drop the file from the file Manager (Explorer) to the program window.
* Double click on the file of the registered type (JPS or MPO).
* Through the context menu "Open with ..." in Explorer.

<a name="file-open-separate"></a>
## Opening a Stereo Pair From a Separate File ##

If the Stereo pair is stored in separate files for the left and right eyes, you can download it one way:

* Through the "file" menu - > ” open separate files " or The ![Открыть]({{ site.baseurl }}/images/document-open-separate.svg){:width="16px" :height="16px"} button on the toolbar.  
In the window that opens, first select the image file for the left eye and then for the right eye.
* Highlighting in Explorer 2 file and dragging them into the program window.
* Through the context menu "Open with ..." in Explorer, selecting 2 files.

<a name="source-format"></a>
## Format of the Source ##

Supported stereo formats:

* Row Interlaced;
* Column Interleaved;
* Anaglyph Red/Cyan;
* Anaglyph Yellow/Blue;
* Side by Side;
* Anamorphic Side by Side;
* Over/Under;
* Anamorphic Over/Under;
* Mono;

The file format is determined automatically in the following cases:

* The MPO file is opened.
* A video file with two video streams is opened.
* The JPS or JPEG file contains a stereo handle.
* The video file contains metadata describing the stereo format.

If the format is not automatically defined, you can select it from the source Format menu.

![Стерео-форматы](../images/screenshorts/stereophotoview-formats.png){:width="500px"}

