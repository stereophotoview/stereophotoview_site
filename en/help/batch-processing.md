---
layout: help
title: Batch Processing of Stereo Photos
lang: en
parent: help
ref: help/batch-processing
permalink: en/help/batch-processing.html
order: 5
subrefs: [['intro','Introduction'], ['job-list','Job List'],['job-settings','Job Settings']]

---

## Batch Processing of Stereo Photos ##

<a name="intro"/>
### Introduction ###

Batch processing allows you to process many photos at a time, which can be useful for automating the processing of many files, for example, when preparing photos for printing, or publishing on the Internet.

<a name="job-list"/>
### Job List ###

In the left part of the window there is a table with a job list. Each job displays one or two source files, a destination file that is generated from a template, and the result of the task.

You can add a job to the queue in the following ways:

* By clicking the "Add sources" or "Add second sources" button under the job list.
* By dragging and dropping files from the file Manager into the batch window.
* By sending files to batch processing stereo photos through the context menu of Windows Explorer.
* By opening multiple files in the Linux file Manager using the "batch stereo photo processing" program.

<a name="job-settings"/>
### Job Settings ###

In the right part of the window there are settings of job parameters. You can save the selected settings as a preset so that you do not have to configure the same settings every time.

#### Default Source Format ####

This setting is only used for sources as a single jpeg file without a stereo descriptor in order to set its stereo format. \
In other cases, the file is automatically detected. If two jpeg files are specified, the first is taken as a picture for the left eye, the second as a picture for the right eye.

#### Actions ####

In this section, you can choose what actions you want to take with the file before saving. Only one action is currently available: auto-align.
The program will try to find similar points in the selected evaluation area, and combine them so that they are visually on the plane of the screen. \
For more information about aligning stereo shots, see the page [Editing 3D Photos and Videos](edit.html#edit-align).

#### Save options ####

The format section specifies the container format, the frame layout for jpeg or jpg files, the target photo size, and the JPEG compression quality. \
The Location section specifies the folder where the results and the result file name template will be saved.

![Batch processing of stereo photos](../images/screenshorts/batch-processing.png){:width="600px"}
