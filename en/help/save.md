---
layout: help
title: Saving a Stereo Pair or Video File
lang: en
parent: help
ref: help/save
permalink: en/help/save.html
order: 4
subrefs: [["save-images","Saving Images"],["save-video","Saving Videos"],["save-freeze-frame","Saving a Freeze Frame from a Video"]]

---
## Saving a stereo pair or video file ##

<a name="save-images"></a>
### Saving Images ###

Edited images can be saved in the MPO format or JPS (JPEG).

To save a file with the same name, select File → Save.

To save with a different name or in a different stereo format, perform "File" → "Save as...".  
When you do this, you will first see a window to select a new file name, and then the file settings window.  
In the window that opens, select the angle location and frame resolution.  
By default, the first view is left. If necessary, you can change this by checking "Reverse order".  

When saving, a stereo descriptor is written to the JPS file, so that it is possible to automatically determine the source format when opening such a file.

<a name="save-video"></a>
### Saving Videos ###

The edited video can only be saved to another file.

To do this, select File → Save as....

Choose a stereo format, similar to saving an image, and video encoding options.

The constant rate factor is used as the CRF parameter for the H264 codec, and to calculate the bitrate for the other codecs.  
In the second case, the target bitrate is calculated by the formula:  

Target-bitrate = (100 - CRF) × raw-bitrate / 100, where raw-bitrate = width × height × 24 × frame-rate.

Codecs and the possibility of storing the descriptions of the stereo format for the different types of video file:

Container | Video Codec | Audio Codec | Stereo Format Description
----------|-------------|-------------|------------------------
MP4       | H264        | AC3         | Yes
MOV       | H264        | AC3         | Yes
Matroska  | H264        | AC3         | Yes
AVI       | MPEG4       | MP3         | --
FLV       | FLV         | MP3         | --

Stored MP4 files is automatically defined as the 3D content with the LG TV.

<a name="save-freeze-frame"></a>
### Saving a Freeze Frame from a Video ###

To save the freeze frame, run the command "File" → "Save a freeze frame as..." or click ![Стоп-кадр]({{ site.baseurl }}/images/camera-photo.svg){:width="16px" :height="16px"} button.

This opens the [save image window](#save-images) where you can select file name and the desired stereo format to save the current video frame.
