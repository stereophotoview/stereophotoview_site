---
layout: help
title: Command Line Mode
lang: en
parent: help
ref: help/command-line
permalink: en/help/command-line.html
order: 6
subrefs: [["Introduction","Introduction"],["command-line-examples", "Examples"], ["command-line-group-examples", "Examples of Group Operations"]]

---

<a name="command-line"></a>
## Command Line Mode ##

<a name="Introduction"></a>
### Introduction ###

Command-line mode can be useful for automating the processing of multiple files, such as preparing for printing, or publishing to the Internet.

To do this, the application includes the command-line utility stereo-conv.

In Windows, when installed, the path to the application folder is automatically added to the PATH variable. In order for these changes to take effect, you need to re-login.

Running with the --help option, or without options, displays usage help. Example:

```
$ stereo-conv --help

Usage: stereo-conv [options] input input2
Converter for stereoscopic photos and videos.
https://stereophotoview.bitbucket.io/en                                                                                    
                                                                                                                           
Options:                                                                                                                   
  -h, --help                 Displays this help.                                                                           
  -v, --version              Displays version information.                                                                 
  -o, --output <file>        Output file.                                                                                  
  --input-layout <layout>    Input stereo layout (see --layouts) - optinal.                                                
  --input-revert <1|0>       Right-eye view first in the input file - optional.                                            
  --output-layout <layout>   Output stereo layout (see --layouts). Default -                                               
                             Monoscopic.                                                                                   
  --output-revert <1|0>      Right-eye view first in the output file -                                                     
                             optional.                                                                                     
  --size <width*height>      Output image size.
  --jpeg-quality <0..100>    Jpeg quality.
  --crf <1..51>              Constant rate factor. Default - 23.
  --preset <preset>          Preset for H264 codec (see --presets). Default -
                             medium.
  --operations <operations>  List of operations separated by a semicolon (see
                             --help-operations).
  -p, --progress             Display progress when saving video.
  -l, --layouts              Display a list of layouts.
  --video-formats            Display a list of video formats.
  --presets                  Display a list of presets.
  --help-operations          Display a list of operations.

Arguments:
  input                      Input file.
  input2                     Optional second input file to load a separate
                             stereo pair.
```

<a name="command-line-examples"></a>
### Examples ###

#### Extracting Left View From MPO File ####

Convert ```src.mpo``` to ```src-left.jpg```.  
Default output format for JPEG files is Mono.

```
stereo-conv -o src-left.jpg src.mpo
```

#### Extracting Right View From MPO File ####

Convert ```src.mpo``` to ```src-left.jpg```.  
Default output format for JPEG files is Mono.  
Determine the reverse order of the angles to extract the right angle: ```--output-revert 1```.

```
stereo-conv --output-revert 1 -o src-right.jpg src.mpo
```

#### Сonversion of Two Separate Images to Anaglyph ####

Convert ```src-left.jpg``` and ```src-right.jpg``` to the ```anaglyph.jpg```.  
Specify the output format ```AnaglyphRC```.

```
stereo-conv --output-layout AnaglyphRC -o anaglyph.jpg src-left.jpg src-right.jpg
```

#### Convertion of Side-by-Side Stereo Pair to MPO File ####

Convert ```src.jps``` with side-by-side stereo pair to the ```result.mpo``` file.  
Specify the input format ```Horizontal```. The output format is determined by the file extension.

```
stereo-conv --input-layout Horizontal -o result.mpo src.jps
```

#### Сonversion of Two Separate Images to MPO File with Automatic Horizontal and Vertical Alignment, and Rotation ####

Convert ```src-left.jpg``` and ```src-right.jpg``` to ```result.mpo```, perform operation ```auto-align:hvr```.

```
stereo-conv --operations auto-align:hvr -o result.mpo src-left.jpg src-right.jpg
```

#### Convertion From MPO File to an Interlaced Stereo Pair With a Frame Fize of 1920x1080: ####

Convert ```src.mpo``` to ```interlaced.jpg```, specify output format ```RowInterlaced``` and size ```--size 1920*1080```.

```
stereo-conv --output-layout RowInterlaced -o interlaced.jpg --size 1920*1080 src.mpo
```

#### Conversion a File Containing Two Video Streams to Anamorphic Side-By-Side Format ####

Convert ```source.avi``` to ```result.mp4``` specify ```AnamorphHorizontal``` output format.  
The input format is determined automatically by the presence of two video streams.

```
stereo-conv --output-layout AnamorphHorizontal -p -o result.mp4 src.avi
```

#### Extracting Left Angle from Anamorphic Side-By-Side Video File ####

Extract video stream with left view from the ```src.mp4``` to the ```src-left.mp4```.   
Specify input format ```AnamorphHorizontal``` and display of progress ```-p```.

```
stereo-conv --input-layout AnamorphHorizontal -p -o src-left.mp4 src.mp4
```

<a name="command-line-group-examples"></a>
### Examples of Group Operations ###

#### Create MPO Files from All the Separate Images for the Left and Right Eyes ####

Windows:

Images for the left eye are stored in files named ```*-left.jpg```, images for the right eye -- in files named ```-right.jpg```.  

* stereo-conv is executed for each file ```*- left.jpg``` in loop ```for %f in (*-left.jpg)```.  
* First, save the file name in the variable F: ```set "F=%f"```.  
* For the first input file, use the value ```%F%``` without change.  
* For the second input file, replace the substring "-left" by "-right": ```%F:-left=-right%```.  
* For the output file, replace the substring "-left.jpg" by ".mpo": ```%F:-left.jpg=.mpo%```.

```
for %f in (*-left.jpg) do (set "F=%f" & stereo-conv -o "%F:-left.jpg=.mpo%" "%F%" "%F:-left=-right%")
```

Linux:

```
for f in *-left.jpg; do stereo-conv -o ${f/-left.jpg/.mpo} $f ${f/-left/-right}; done
```

#### Extract Left Angles From the All MPO Files ####

Convert all files with the extension ```mpo``` into files with the same name, adding the ending ```-left.jpg```, and save to the folder ```separate```.  
The input and output formats are determined automatically by extension.  
You must first create the "separate" folder.

Windows:

```
for %f in (*.mpo) do stereo-conv -o separate/%f-left.jpg %f
```

Linux:

```
for f in *.mpo; do stereo-conv -o separate/${f/.mpo/-left.jpg} $f; done
```
 
#### Extract Right Angles from the All MPO Files ####

Convert all files with the extension ```mpo``` into files with the same name, adding the ending ```-right.jpg```, and save to the folder ```separate```.  
The input and output formats are determined automatically by extension.  
In order to extract the right angle, specify the reverse order of angles ```--output-revert 1```.  
You must first create the "separate" folder.

Windows:

```
for %f in (*.mpo) do stereo-conv --output-revert 1 -o separate/%f-right.jpg %f
```

Linux:

```
for f in *.mpo; do stereo-conv --output-revert 1 -o separate/${f/.mpo/-right.jpg} $f; done
```
        
#### Automatic Vertical Alignment of All MPO Files ####

Convert all files with the extension ```mpo``` into files with the same name in the folder ```result```, and perform the operation ```auto-align:v```.  
You must first create the "result" folder.

Windows:

```
for %f in (*.mpo) do stereo-conv --operations auto-align:v -o result/%f %f
```

Linux:

```
for f in *.mpo; do stereo-conv --operations auto-align:v -o result/$f $f
```
