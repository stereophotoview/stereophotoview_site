---
layout: help
title: Viewing Stereoscopic Content
lang: en
parent: help
ref: help/view
permalink: en/help/view.html
order: 2
subrefs: [["format","Display Format"],["nav","Navigate Files"],["filesystem","Working with the File System"],["start-screen","Start Screen"]]

---

## Viewing Stereoscopic Content ##

<a name="format"></a>
### Display Format ###

To play the stereo effect correctly, you need to select the display format that matches your display.

If you are using a monitor or TV with passive polarization technology (e.g. LG Cinema 3D), use the Row Interlaced format. At the same time, the native resolution of the monitor must be set in the OS settings.

If you are using a normal monitor and anaglyph glasses, select red-blue or yellow-blue anaglyph.

Additionally, there are various options for the location of the angles on the frame.

### Control ###

You can control the view through the View menu or through the context menu of the image, which is opened by right-clicking on the image.
Keyboard shortcuts for actions are displayed in tooltips.

### Video Playback ###

To control video playback, when the mouse pointer moves over the frame, a special panel appears that allows you to pause playback, change the current position, and change the volume.

<a name="nav"></a>
## Navigate Files ##

StereoPhotoView allows you to navigate through the files in the current image folder with the PageUp, PageDown, Home, and End keys.

To display the files in the folder of the current image, select menu "View" → "Thumbnail bar (T)". In full screen mode, you can simply move the mouse pointer over the top of the screen.

When opening stereo pairs from separate files, navigation will be possible if stereo pairs are repeated in the same directories at certain intervals.

<a name="filesystem"></a>
## Working with the File System ##

You can delete, move, or copy a currently opened file to the trash.

<a name="start-screen"></a>
## Start Screen ##

When the application starts, or when the file is closed, the application displays a folders tree, a list of favorite folders, and a list of recent folders.

<a name="folders-tree"></a>
### Folders Tree ###

The computer's file system directory tree is displayed on the bottom left of the start screen. The contents of the selected folder are displayed on the right.

<a name="favorites"></a>
### Favorite Folders ###

The top left of the home screen displays a list of your favorite folders.

To add a folder to your favorites list, click the folder on the right side of the screen and select "Add to favorites".

To remove a folder from favorites, right-click the desired folder in the list of "favorite folders" and select "Delete".

<a name="last-folders"></a>
### Recent Folders ###

Select "Recent folders" from the Favorite folders list to open the list of folders from which the most recent files were opened.

To delete one or all folders from the list of recent folders, right - click on the folder and select the appropriate menu item.

