---
layout: default
title: 'Support the Project'
lang: en
ref: support-the-project
permalink: en/support-the-project.html
order: 5

---

# Support the Project

StereoPhotoView is an open-source project developed in free time and driven by pure enthusiasm.

Any help to the project is greatly appreciated, and there are various ways you can contribute based on your capabilities and interest. Here are just a few of them:

## Spread the Word about StereoPhotoView

If you've enjoyed using the program, you can tell your friends or followers about it, write a guide, or something else creative.

## Report Issues or Suggest New Features

If you come across any bugs or have suggestions for improving the program, please add them to the [project's issue page]({{site.bitbucket}}/issues?status=new&status=open).

## Support for Linux Packages

StereoPhotoView is available as a self-contained AppImage package and a package for Ubuntu in third-party repositories. You can make the program more accessible to users of other distributions by becoming a package maintainer for them or creating a self-contained package.

## Creating a Distribution for Mac OS

If you're a Mac OS user with experience in building software for it, you could share the program with other users by creating a distribution for this operating system.

## Localization

Currently, the program is available in English and Russian. I'm not an expert, so there are likely translation errors that you could help fix. If you want to expand the list of available languages, you can do so. Please reach out to me to discuss the implementation methods.

## Developing StereoPhotoView

If you have programming experience or the desire to get involved, you can contribute by fixing issues or adding new features.

The program is written in C++ using the QT framework, and the source code is available on [Bitbucket]({{site.bitbucket}}). For implementation, you can choose from the [registered tasks]({{site.bitbucket}}/issues?status=new&status=open) or come up with your own ideas.