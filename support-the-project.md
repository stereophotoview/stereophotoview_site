---
layout: default
title: 'Поддержать проект'
lang: ru
ref: support-the-project
permalink: support-the-project.html
order: 5

---

# Поддержать проект

StereoPhotoView - это проект с открытым исходным кодом, который разрабатывается в свободное время на чистом энтузиазме.

Любая помощь проекту может только приветствоваться, а сделать это можно разными способами, в зависимости от ваших возможностей и желания. \
Вот только некоторые из них:

## Расскажите о StereoPhotoView

Если программа вам понравилась, вы можете рассказать о ней своим друзьям или подписчиками, написать руководство, или что-нибудь ещё.

## Сообщите о проблемах или предложите новую функциональность ##

Если вы заметили ошибку или у вас есть предложения по улучшению программы, добавьте их на [страницу проекта]({{site.bitbucket}}/issues?status=new&status=open).

## Поддержка пакетов Linux

StereoPhotoView доступна в качестве самодостаточного пакета AppImage и пакета для Ubuntu в репозитории сторонних пакетов. Вы можете сделать программу доступнее пользователям других дистрибутивов, став сопровождающим пакетов для них или какого-нибудь самодостаточного пакета.

## Создание дистрибутива для Mac OS

Если вы пользователь Mac OS, и имеете опыт сборки программ под него, вы могли бы поделиться программой с другими пользователями, создав дистрибутив под эту операционную систему.

## Локализация

Сейчас программа доступна на английском и русском языках. Я не являюсь специалистом, и наверняка в переводе есть ошибки, которые вы могли бы исправить. Если вы хотите расширить список доступных языков, вы можете это сделать. Пожалуйста, свяжитесь со мной для обсуждения способов реализации этого.

## Развитие программы StereoPhotoView

Если у вас есть опыт в программировании, или желание им заняться, вы можете заняться исправлением проблем или добавлением новой функциональности.

Программа написана на C++ с использованием фреймворка QT, а исходные коды доступны на платформе [Bitbucket]({{site.bitbucket}}).
Для реализации вы можете выбрать одну из [зарегистрированных задач]({{site.bitbucket}}/issues?status=new&status=open), или придумать свою.
