# Jekyll sources for stereophotoview.bitbucket.io site

# Installing dependencies

For debian based linux:

```
sudo apt install ruby-dev
sudo gem install bundler
bundler install
```

# Starting the server for debugging

```
bundle exec jekyll serve --config _config.yml,_config_dev.yml
```

# Build site for release

```
bundle exec jekyll build --config _config.yml,_config_prod.yml -d ../stereophotoview.bitbucket.org
```
