---
layout: default
title: 'Контакты'
lang: ru
ref: contacts
permalink: contacts.html
order: 6
---

Контакты
========

Автор: Александр Мамзиков\\
E-mail: [av.mamzikov@gmail.com](mailto:av.mamzikov@gmail.com)\\
Bitbucket: [{{site.bitbucket}}]({{site.bitbucket}})
