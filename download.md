---
layout: default
title: 'Скачать'
lang: ru
ref: download
permalink: download.html
order: 2
---
{% capture download_header %}Загрузка{% endcapture download_header %}
{% capture download %}Скачать{% endcapture %}
{% assign Installer='Установщик' %}
{% assign Portable='Переносимый' %}
{% assign any_linux='Любой Linux' %}
{% assign appimage_note='Для запуска приложения, просто скачайте этот файл, сделайте исполняемым, и выполните.\
Для интеграции в систему можно использовать [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher).' %}
{% assign additionalDownloads='Дополнительные загрузки' %}
{% assign consoleConverter='Консольный конвертер' %}
{% assign bit32='32 бит' %}
{% assign bit64='64 бит' %}
{% assign otherVersions='Другие версии' %}
{% assign debian_based='Ubuntu' %}
{% assign stableVersionOn='Последняя стабильная версия доступна на' %}
{% assign installCommands='Для установки, выполните следующие команды:' %}
{% assign instructions='Инструкции' %}
{% assign rosa_repo='Пакеты для [ROSA](https://www.rosalinux.ru/) предоставил [djam](https://abf.io/djam) в своём [персональном репозитории](https://abf.io/platforms/djam_personal), где есть сборки под i586 и x86_64 для версии ROSA Fresh R8. ' %}
{% assign rosa_example='Пример установки пакета в 64-х разрядной версии ROSA Fresh R8:' %}
{% assign rosa_link='Ссылку для добавления репозитория в вашу версию системы смотрите на странице [репозитория](https://abf.io/platforms/djam_personal).' %}
{% assign sources='Исходный код' %}
{% assign archives='Архивы' %}
{% assign gitRepo='Git репозиторий' %}
{% assign lastStableVersion='Последняя стабильная версия' %}
{% assign buildingManual='Инструкция по сборке' %}
{% assign version='Версия' %}
{% assign readme_file='README_RU.md' %}
{% include download.md %}
