---
layout: help
title: Режим командной строки
lang: ru
parent: help
ref: help/command-line
permalink: help/command-line.html
order: 5
subrefs: [["Introduction","Введение"],["command-line-examples", "Примеры"], ["command-line-group-examples", "Примеры групповых операций"]]

---

<a name="command-line"></a>
## Режим командной строки ##

<a name="Introduction"></a>
### Введение ###

Режим командной строки может быть полезен для автоматизации обработки множества файлов, например, при подготовке для печати, или публикации в интернете.

Для этого в состав приложения входит утилита командной строки stereo-conv.

В Windows, при установке, путь к папке приложения автоматически добавляется в переменную PATH. Для того, чтобы эти изменения вступили в силу, нужно пере-зайти в систему.

Запуск с параметром --help, или без параметров, отображает справку по использованию. Пример:

```
$ stereo-conv --help

Usage: stereo-conv [options] input input2
Конвертер для стереоскопических фотографий и видео файлов.
https://stereophotoview.bitbucket.io/

Options:
-h, --help                 Displays this help.
-v, --version              Displays version information.
-o, --output <file>        Выходной файл.
--input-layout <layout>    Компоновка стерео-пары во входном файле (см.
--layouts) - не обязательный.
--input-revert <1|0>       Указывает, что во входном файле сначала идёт
ракурс для правого глаза - не обязательно.
--output-layout <layout>   Компоновка стерео-пары в выходном файле (см.
--layouts) - По умолчанию - Monoscopic.
--output-revert <1|0>      Указывает, что в выходном файле сначала идёт
ракурс для правого глаза - не обязательно.
--size <width*height>      Размер кадра в выходном файле.
--jpeg-quality <0..100>    Качество JPEG.
--crf <1..51>              Постоянный коэффициент скорости для сохранения
видео. По умолчанию - 23.
--preset <preset>          Пресет для кодека H264 (см. --presets). По
умолчанию - medium.
--operations <operations>  Список операций, разделённый точкой с запятой (см.
--help-operations).
-p, --progress             Отображать прогресс сохранения видео.
-l, --layouts              Отобразить список возможных компоновок стерео-пар.
--video-formats            Отобразить список форматов видео файлов.
--presets                  Отобразить список пресетов.
--help-operations          Отобразить список операций.

Arguments:
input                      Входной файл.
input2                     Необязательный второй входной файл для загрузки
раздельной стерео-пары.
```

<a name="command-line-examples"></a>
### Примеры ###

#### Извлечение левого ракурса из MPO файла ####

Открыть ```src.mpo``` и сохранить в ```src-left.jpg```.   
Формат вывода по умолчанию для JPEG файлов - моно.

```
stereo-conv -o src-left.jpg src.mpo
```

#### Извлечение правого ракурса из MPO файла ####

Открыть ```src.mpo``` и сохранить в ```src-right.jpg```.   
Формат вывода по умолчанию для JPEG файлов - моно.   
Для того, чтобы сохранить именно правый ракурс, указываем обратный порядок ракурсов ```--output-revert 1```.

```
stereo-conv --output-revert 1 -o src-right.jpg src.mpo
```

#### Преобразование двух отдельных снимков в анаглиф ####

Преобразовать ```src-left.jpg``` и ```src-right.jpg``` в ```anaglyph.jpg```.   
Указываем формат вывода ```AnaglyphRC```.

```
stereo-conv --output-layout AnaglyphRC -o anaglyph.jpg src-left.jpg src-right.jpg
```

#### Преобразование горизонтальной стерео-пары в файл MPO ####

Преобразовать стереопару ```src.jps``` в файл ```result.mpo```.  
Указываем входной формат ```Horizontal```. Выходной формат определяется по расширению файла.

```
stereo-conv --input-layout Horizontal -o result.mpo src.jps
```

#### Преобразование двух отдельных снимков в MPO файл с автоматическим выравниванием по горизонтали, вертикали, и углу наклона ####

Преобразовать ```src-left.jpg``` и ```src-right.jpg``` в ```result.mpo```, выполнив операцию ```auto-align:hvr```.

```
stereo-conv --operations auto-align:hvr -o result.mpo src-left.jpg src-right.jpg
```

#### Преобразование MPO файла в чересстрочную стереопару размером 1920x1080 ####

Преобразовать ```src.mpo``` в ```interlaced.jpg``` с указанием выходного формата ```RowInterlaced``` и размера ```--size 1920*1080```.

```
stereo-conv --output-layout RowInterlaced -o interlaced.jpg --size 1920*1080 src.mpo
```

#### Преобразование видео с раздельными видео-потоками в видео файл с анаморфной горизонтальной стерео-парой ####

Преобразовываем ```src.avi``` в ```result.mp4```, и указываем выходной формат ```AnamorphHorizontal```.  
Входной формат определяется автоматически по наличию двух видео-потоков.

```
stereo-conv --output-layout AnamorphHorizontal -p -o result.mp4 src.avi
```

#### Извлечение левого ракурса из видео-файла с анаморфной горизонтальной стерео-парой: ####

Преобразовываем ```src.mp4``` в ```src-left.mp4```.  
Указываем входной формат ```AnamorphHorizontal``` и отображение прогресса выполнения ```-p```.

```
stereo-conv --input-layout AnamorphHorizontal -p -o src-left.mp4 src.mp4
```

<a name="command-line-group-examples"></a>
### Примеры групповых операций ###

#### Создать mpo файлы из всех отдельных снимков для левого (*-left.jpg) и правого (*-right.jpg) глаза ####

Изображения для левого глаза хранятся в файлах с именем ```*-left.jpg```, изображения для правого глаза - в файлах с именем ```*-right.jpg```.  
Результат сохраняется в файл с именем ```*.mpo```.

Windows:

```stereo-conv``` выполняется для каждого файла ```*-left.jpg``` в цикле ```for %f in (*-left.jpg)```.  
* Сначала сохраняем имя файла в переменную F: ```set "F=%f"```.  
* Для первого входного файла используем значение ```%F%``` без преобразований.  
* Для второго входного файла заменяем подстроку "-left" на "-right": ```%F:-left=-right%```.  
* Для выходного файла заменяем подстроку "-left.jpg" на ".mpo": ```%F:-left.jpg=.mpo%```.

```
for %f in (*-left.jpg) do (set "F=%f" & stereo-conv -o "%F:-left.jpg=.mpo%" "%F%" "%F:-left=-right%")
```
    
Аналогично для Linux:

```
for f in *-left.jpg; do stereo-conv -o ${f/-left.jpg/.mpo} $f ${f/-left/-right}; done
```


#### Извлечь левые ракурсы из всех MPO файлов в папку separate ####

Преобразовываем все файлы с расширением ```mpo``` в файлы с таким же именем, добавив окончание ```-left.jpg```, и сохраняем в папку ```separate```.  
Входной и выходной форматы определяются автоматически по расширению.   
Папку нужно предварительно создать.

Windows:

```
for %f in (*.mpo) do stereo-conv -o separate/%f-left.jpg %f
```

Linux:

```
for f in *.mpo; do stereo-conv -o separate/${f/.mpo/-left.jpg} $f; done
```

#### Извлечь правые ракурсы из всех MPO файлов в папку separate ####

Преобразовываем все файлы с расширением ```mpo``` в файлы с таким же именем, добавив окончание ```-left.jpg```, и сохраняем в папку ```separate```.  
Входной и выходной форматы определяются автоматически по расширению.  
Для того, чтобы сохранить именно правый ракурс, указываем обратный порядок ракурсов ```--output-revert 1```.  
Папку нужно предварительно создать.

Windows:

```
for %f in (*.mpo) do stereo-conv --output-revert 1 -o separate/%f-right.jpg %f
```

Linux:

```
for f in *.mpo; do stereo-conv --output-revert 1 -o separate/${f/.mpo/-right.jpg} $f; done
```

#### Автоматическое выравнивание по вертикали всех mpo файлов и сохранение результата в папку result ####

Преобразовываем все файлы с расшрением ```mpo``` в файлы с таким-же именем в папку ```result```, и выполняем операцию ```auto-align:v```.  
Папку нужно предварительно создать.

Windows:

```
for %f in (*.mpo) do stereo-conv --operations auto-align:v -o result/%f %f
```

Linux:

```
for f in *.mpo; do stereo-conv --operations auto-align:v -o result/$f $f
```
