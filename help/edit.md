---
layout: help
title: Редактирование 3D стерео фотографий и видео
lang: ru
parent: help
ref: help/edit
permalink: help/edit.html
order: 3
subrefs: [["edit-align", "Выравнивание"], ["edit-auto-align", "Авто-выравнивание"],["edit-extended-auto-align","Расширенное авто-выравнивание"],["edit-variable-align","Переменная глубина сцены стереоскопического видео контента"],["edit-crop","Кадрирование"],["edit-external","Открытие ракурсов во внешнем редакторе"]]

---

<a name="edit"></a>
## Редактирование 3D стерео фотографий и видео ##

<a name="edit-align"></a>
### Выравнивание ###

![Выравнивание]({{ site.baseurl }}/images/screenshorts/stereophotoview-align.png){:width="500px"}

Инструмент "Выравнивание" открывается через меню "Правка" → "Выравнивание" или клавишей A (англ).

Для выравнивания по вертикали используется вертикальный ползунок справа и клавиши: вверх и вниз.  
Для горизонтального - ползунок в нижней части экрана и клавиши: влево и вправо.  
Если нажимать эти клавиши вместе с Ctrl, ракурсы будут двигаться большими шагами.

Если ракурсы сняты фотоаппаратом с рук, может понадобится повернуть ракурсы. Это делается с помощью ползунков и спинеров в верхней части изображения.

Для горизонтального выравнивания (настройки параллакса), объект, который будет визуально находится на плоскости экрана нужно выравнять так, чтобы на левом и правом ракурсе, он находился в одном месте экрана (не двоился). При этом объекты, находящиеся за ним, будут восприниматься, глазом так, как будто они находятся за экраном, а те объекты, которые находятся перед ним, будут визуально располагаться перед экраном.

Изображения левого глаза на выступающих объектах находится правее изображения для правого. То есть, для их просмотра нужно сводить глаза к переносице. Для комфортного просмотра, не рекомендуется допускать слишком большое расстояние между изображениями этой детали на разных ракурсах (сильного двоения).

По вертикали, нужно просто совместить левый и правый ракурс так, чтобы объекты находились на одном уровне.

После того, как вы добьётесь желаемого результата, нажмите кнопку "Применить".

<a name="edit-auto-align"></a>
### Авто-выравнивание ###

Выполните меню "Правка" → "Авто-выравнивание" или нажмите Ctrl+A (англ).

Затем, с помощью мыши, выберите на изображении квадратную область, которая будет визуально находится на плоскости экрана. Для изменения размеров области, используйте колёсико мыши.

Нажмите левую кнопку мыши. Программа выполнит поиск ключевых точек в выбранной области, и выравняет изображение по ним.


<a name="edit-extended-auto-align"></a>
### Расширенное авто-выравнивание ###

Данный инструмент запускается через меню "Правка" → "Расширенное авто-выравнивание" или сочетанием Ctrl+E,A (англ).

Программа находит на обоих снимках похожие ключевые точки и вычисляет необходимую величину сдвига по вертикали, горизонтали, а также углы поворота ракурсов.
После этого открывается инструмент выравнивание, авто-выравнивания, для ручной корректировки.

В меню "Правка" → "Настройки…" можно выбрать, какие параметры будут вычисляться автоматически: параллакс, вертикальное смещение, вращение.

<a name="edit-variable-align"></a>
### Переменная глубина сцены стереоскопического видео контента ###

В стереоскопическом видео контенте может меняться расстояние до основного объекта.
В таких случаях, для комфортного просмотра, нужно изменять глубину сцены в соответствии с расстоянием до объекта.  
На примере временного приближения объекта, сделать это можно следующим образом.

*1. Настройте базовую глубину сцены.*  
Для этого просто воспользуйтесь одним из инструментов: [Выравнивание](#edit-align), [Авто-выравнивание](#edit-auto-align), или [Расширенное авто-выравнивание](#edit-extended-auto-align).

*2. Поставьте временные метки в ключевых точках.*  
Отметьте следующие моменты: где основной объект начинает приближаться, где уже приблизился, где начинает отдаляться, и где уже отдалился.  
В каждый такой момент времени поставьте паузу и нажмите Ctrl+T (или выберите меню "Правка" → "Поставить метку").

*3. Настройте глубину сцены в ключевых точках.*  
Выберите на шкале времени метку, где объект уже приблизился и настройте необходимую глубину сцены для него, воспользовавшись одним из инструментов выравнивания.
Повторите тоже самое в момент времени, когда объект начинает отдаляться.

В результате, в процессе воспроизведения, глубина сцены плавно меняется при изменении расстояния до объекта.  
Результат можно [сохранить](save.html#save-video) в новый видео файл.

<a name="edit-crop"></a>
### Кадрирование ###

Инструмент "Кадрирование" открывается через меню "Правка" → "Кадрировать" или клавишей C (англ).

Выберите область снимка, и нажмите кнопку "Кадрировать".

В нижней панели также можно выбрать желаемое соотношение сторон кадра.

![Кадрирование]({{ site.baseurl }}/images/screenshorts/stereophotoview-crop.png){:width="500px"}

<a name="edit-external"></a>
### Открытие ракурсов во внешнем редакторе ###

Эта функция может быть полезна, например, для удаления эффекта красных глаз во внешнем редакторе.

1. В меню "Правка" → "Открыть левый ракурс в…" выберите редактор.  
2. Левый ракурс должен открыться в выбранной программе.  
3. После редактирования, сохранить файл с тем-же именем, вернитесь к окну StereoPhotoView, и нажмите кнопку "Импортировать".

