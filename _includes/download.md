# {{download_header}} #

## Windows ##

### {{Installer}} ###

[{{download}} stereophotoview-{{site.ver}}-setup.exe]({{site.bitbucket}}/downloads/stereophotoview-{{site.ver}}-setup.exe){: .download} - {{bit64}} 

### {{Portable}} ###

[{{download}} stereophotoview-{{site.ver}}.zip]({{site.bitbucket}}/downloads/stereophotoview-{{site.ver}}.zip){: .download} - {{bit64}}

[{{ otherVersions }}]({{site.bitbucket}}/downloads)

## Linux ##

### {{any_linux}} ####

[{{download}} stereophotoview-{{site.ver}}.AppImage]({{site.bitbucket}}/downloads/stereophotoview-{{site.ver}}.AppImage){: .download} - x86_64

{{appimage_note}}

{{additionalDownloads}}:

* {{consoleConverter}} [stereoconv-{{site.ver}}.AppImage]({{site.bitbucket}}/downloads/stereoconv-{{site.ver}}.AppImage);
* [{{ otherVersions }}]({{site.bitbucket}}/downloads).

### {{ debian_based }} ###

{{ stableVersionOn }} [Launchpad](https://launchpad.net/~stereophotoview/+archive/ubuntu/stable). \\
{{ installCommands }}

```
sudo add-apt-repository ppa:stereophotoview/stable
sudo apt-get update
sudo apt-get install stereophotoview
```

### Arch linux ###


{{ stableVersionOn }} [AUR](https://aur.archlinux.org/packages/stereophotoview/). ([{{instructions}}](https://wiki.archlinux.org/index.php/Arch_User_Repository_%28%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9%29)).

### ROSA ###

{{ rosa_repo }} 
{{ rosa_example }}

```
urpmi.addmedia djam_personal http://abf-downloads.rosalinux.ru/djam_personal/repository/rosa2014.1/x86_64/main/release
urpmi stereophotoview
```

{{ rosa_link }}

## {{ sources}} ##

### {{ archives}} ###

* [{{lastStableVersion}}]({{site.bitbucket}}/get/master.zip)
* [{{version}} {{site.ver}}]({{site.bitbucket}}/get/{{site.ver}}.zip)

### {{ gitRepo}} ###

```
git clone {{site.bitbucket}}.git
```
[{{buildingManual}}](https://bitbucket.org/stereophotoview/stereophotoview/src/master/{{readme_file}}).
